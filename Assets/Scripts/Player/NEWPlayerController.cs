﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class NEWPlayerController : MonoBehaviour
{
    [SerializeField] private float movementSpeed = 5;
    private Controls controls = null;
    // Start is called before the first frame update
    void Awake()
    {
        controls = new Controls();
    }

    private void OnEnable()
    {
        controls.Player.Enable();
    }
    private void OnDisable()
    {
        controls.Player.Disable();
    }


    // Update is called once per frame
    void Update()
    {
        move();
        
    }

    private void move()
    {
        Vector3 movementInput = controls.Player.Movement.ReadValue<Vector2>();           
        movementInput.z = movementInput.y;
        movementInput.y = 0;


        //if (Keyboard.current.wKey.isPressed) { movementInput.z += 1; }
        //if (Keyboard.current.aKey.isPressed) { movementInput.x -= 1; }
        //if (Keyboard.current.sKey.isPressed) { movementInput.z -= 1; }
        //if (Keyboard.current.dKey.isPressed) { movementInput.x += 1; }

        movementInput.Normalize();

        transform.Translate(movementInput * movementSpeed * Time.deltaTime);
    }
}

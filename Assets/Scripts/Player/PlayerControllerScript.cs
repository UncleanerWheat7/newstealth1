﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;

public class PlayerControllerScript : MonoBehaviour
{
    public State state;

    public Transform mainCameraTransform;
    public CinemachineFreeLook mainCamera, CoverCamera;
    private float rayCastHight = 1;
    private float rayCastLength = 5f;
    public LayerMask layerMask;

    Vector3 wallrunVector;

    Vector3 movementInput;
    Vector3 forward;
    Vector3 right;
    Vector3 desiredMoveDirection;
    Vector3 gravityVector;
    float gravity = 3;

    private CharacterController controller;
    private Animator anim;

    private float currentSpeed = 0;
    private float moveSpeed;
    private float maxMoveSpeed = 5;
    private float speedSmoothVelocity = 0.1f;
    private float speedSmoothTime = 0.1f;
    private float rotationSpeed = 0.9f;
    private Controls controls = null;
    bool wallHug = false;

    #region setupController
    void Awake()
    {
        controls = new Controls();
        controls.Player.WallHug.performed += _ => WallLeanCheck();
    }
    private void OnEnable()
    {
        controls.Player.Enable();
    }
    private void OnDisable()
    {
        controls.Player.Disable();
    }
    #endregion setupController

    void Start()
    {
        mainCameraTransform = Camera.main.transform;
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        moveSpeed = maxMoveSpeed;
        NextState();
    }

    private void Update()
    {
        print(moveSpeed);
    }

    public enum State
    {
        Walk,
        Wall
    }


    IEnumerator WalkState()
    {
        Debug.Log("Walk: Enter");
        while (state == State.Walk)
        {
            getMovementInput();
            calculateWalkInput();
            applyWalkInput();

            yield return 0;
        }
        Debug.Log("Walk: Exit");
        NextState();
    }

    IEnumerator WallState()
    {
        Debug.Log("Wall: Enter");
        while (state == State.Wall)
        {
            getMovementInput();
            calculateWallInput();
            applyWallInput();

            yield return 0;
        }
        Debug.Log("Walk: Exit");
        NextState();
    }

    void NextState()
    {
        string methodName = state.ToString() + "State";
        System.Reflection.MethodInfo info =
            GetType().GetMethod(methodName,
                                System.Reflection.BindingFlags.NonPublic |
                                System.Reflection.BindingFlags.Instance);

        if (state == State.Wall)
        {
            mainCamera.Priority = 9;
            CoverCamera.m_XAxis.Value = 0;
        }
        else
        {
            mainCamera.Priority = 10;
        }
        StartCoroutine((IEnumerator)info.Invoke(this, null));
    }

    public void getMovementInput()
    {
        movementInput = controls.Player.Movement.ReadValue<Vector2>();
        movementInput.z = movementInput.y;

        forward = mainCameraTransform.forward;
        right = mainCameraTransform.right;
        forward.y = 0;
        right.y = 0;
    }


    public void calculateWalkInput()
    {
        desiredMoveDirection = (forward * movementInput.y + right * movementInput.x).normalized;

        if (!controller.isGrounded)
        {
            gravityVector.y = -gravity;
        }
        else
        {
            if (desiredMoveDirection != Vector3.zero)
            {
                anim.speed = movementInput.magnitude;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), rotationSpeed);
                checkForWall();
            }
            else
            {
                anim.speed = 1;
            }
            float targetSpeed = moveSpeed * movementInput.magnitude;
            currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedSmoothVelocity, speedSmoothTime);
        }
    }

    void applyWalkInput()
    {
        anim.SetFloat("Speed", currentSpeed);
        controller.Move(desiredMoveDirection * currentSpeed * Time.deltaTime);
        controller.Move(gravityVector * Time.deltaTime);
    }


    public void calculateWallInput()
    {
        if (!controller.isGrounded)
        {
            gravityVector.y = -gravity;
        }
        else
        {
            if (desiredMoveDirection != Vector3.zero)
            {
                //anim.speed = movementInput.magnitude;
            }
            else
            {
                anim.speed = 1;
            }


            float targetSpeed = moveSpeed * (movementInput.magnitude);
            currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedSmoothVelocity, speedSmoothTime);
        }
    }

    void applyWallInput()
    {
        //transform.Rotate(0, 180, 0);

        anim.SetFloat("Speed", movementInput.x);

        controller.Move(wallrunVector * (currentSpeed * movementInput.x) * Time.deltaTime);
        controller.Move(gravityVector * Time.deltaTime);
    }



    public void checkForWall()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + (Vector3.up * rayCastHight), transform.forward, out hit, rayCastLength, layerMask))//if direction player is heading is into a wall
        {
            print("Press B to hug wall");
        }
    }

    void WallLeanCheck()
    {
        print("Wall 1");
        RaycastHit hit;
        if (!anim.GetBool("WallLean"))//if player is going to wall but not yet in the wall animation
        {
            if (Physics.Raycast(transform.position + (Vector3.up * rayCastHight), transform.forward, out hit, rayCastLength, layerMask))//if direction player is heading is into a wall
            {
                state = State.Wall;
                anim.SetBool("WallLean", true);
                //rayCastLength = 1;
                wallrunVector = transform.right;// Vector3.Cross(hit.normal, transform.up).normalized;

                clampToWall(hit);
            }

        }
        else
        {
            state = State.Walk;
            anim.SetBool("WallLean", false);
            //rayCastLength = 1;

        }
    }


    void clampToWall(RaycastHit hit)
    {
        Vector3 newRotation = new Vector3(0, hit.normal.y, 0);
        transform.rotation = Quaternion.FromToRotation(transform.forward, hit.normal) * transform.rotation;
        Vector3 newPos = new Vector3(hit.point.x, transform.position.y, hit.point.z);
        controller.enabled = false;
        transform.position = newPos;
        controller.enabled = true;
    }





    //private float moveSpeed;
    //private float maxMoveSpeed = 5;
    //private float crouchSpeed = 2;
    //private float wallLeanSpeed = 1;
    //private float currentSpeed = 0;
    //private float speedSmoothVelocity = 0.1f;
    //private float speedSmoothTime = 0.1f;
    //private float rotationSpeed = 0.9f;
    //private float gravity = 3f;
    //public CinemachineFreeLook freeLookCam;
    //public LayerMask layerMask;

    //public float cameraTarget;
    //private float rayCastLength = 0.5f;
    //private float rayCastHight = 1;


    //Vector2 movementInput;
    //Vector3 forward;
    //Vector3 right;
    //Vector3 desiredMoveDirection;
    //Vector3 gravityVector;


    //private Transform mainCameraTransform;
    //private CharacterController controller;
    //private Animator anim;
    //// Start is called before the first frame update
    //void Start()
    //{
    //    mainCameraTransform = Camera.main.transform;
    //    controller = GetComponent<CharacterController>();
    //    anim = GetComponent<Animator>();
    //    moveSpeed = maxMoveSpeed;
    //}

    //// Update is called once per frame
    //void Update()
    //{
    //    Move();//player input for movement    
    //    Action();
    //    Debug.DrawRay(transform.position + (Vector3.up * 1), desiredMoveDirection);
    //}

    //private void Move()
    //{
    //    movementInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
    //    forward = mainCameraTransform.forward;
    //    right = mainCameraTransform.right;
    //    forward.y = 0;
    //    right.y = 0;

    //    forward.Normalize();
    //    right.Normalize();
    //    movementInput = Vector3.ClampMagnitude(movementInput, 1f);// = desiredMoveDirection / desiredMoveDirection.magnitude;

    //    desiredMoveDirection = (forward * movementInput.y + right * movementInput.x).normalized;
    //    gravityVector = Vector3.zero;


    //    if (!controller.isGrounded)
    //    {
    //        gravityVector.y = -gravity;
    //    }
    //    else
    //    {

    //        if (desiredMoveDirection != Vector3.zero)
    //        {
    //            anim.speed = movementInput.magnitude;
    //            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), rotationSpeed);
    //        }
    //        else
    //        {
    //            anim.speed = 1;
    //        }
    //        float targetSpeed = moveSpeed * movementInput.magnitude;
    //        currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedSmoothVelocity, speedSmoothTime);
    //    }


    //    if (anim.GetBool("WallLean"))
    //    {
    //        anim.SetFloat("Speed", Input.GetAxisRaw("Horizontal"));

    //    }
    //    else if (anim.GetBool("Crouch"))
    //    {
    //        anim.SetFloat("Speed", currentSpeed);
    //    }
    //    else
    //    {
    //        if (moveSpeed != maxMoveSpeed)
    //        {
    //            moveSpeed = maxMoveSpeed;
    //            rayCastHight = 1f;
    //        }
    //        anim.SetFloat("Speed", currentSpeed);
    //    }

    //    WallLeanCheck();

    //    if (anim.GetBool("WallLean"))
    //    {
    //        if (Mathf.Abs(desiredMoveDirection.x) > 0.4f)
    //            controller.Move(desiredMoveDirection * currentSpeed * Time.deltaTime);
    //    }
    //    else
    //    {
    //        controller.Move(desiredMoveDirection * currentSpeed * Time.deltaTime);
    //    }



    //    controller.Move(gravityVector * Time.deltaTime);
    //}

    //private void Action()
    //{
    //    if (Input.GetButtonDown("Crouch"))
    //    {
    //        anim.SetBool("Crouch", !anim.GetBool("Crouch"));
    //        StartCoroutine("crouchPercent");

    //        if (anim.GetBool("Crouch"))
    //        {
    //            if (!anim.GetBool("WallLean"))
    //            {
    //                moveSpeed = crouchSpeed;
    //            }
    //            rayCastHight = 0.5f;
    //            controller.height = 1f;
    //            controller.center = new Vector3(0, 0.65f, 0);
    //        }
    //        else
    //        {
    //            if (!anim.GetBool("WallLean"))
    //            {
    //                moveSpeed = maxMoveSpeed;
    //            }
    //            rayCastHight = 1;
    //            controller.height = 1.5f;
    //            controller.center = new Vector3(0, 0.9f, 0);
    //        }
    //    }


    //    if (Input.GetButtonDown("Fire3"))
    //    {
    //        anim.speed = 1;
    //        anim.SetTrigger("Choke");
    //    }

    //    if (Input.GetButtonDown("Fire1"))
    //    {
    //        anim.SetFloat("Shooting", 1);
    //    }
    //    if (Input.GetButtonUp("Fire1"))
    //    {
    //        anim.SetFloat("Shooting", 0);
    //    }
    //}

    //void WallLeanCheck()
    //{
    //    RaycastHit hit;
    //    if (Physics.Raycast(transform.position + (Vector3.up * rayCastHight), desiredMoveDirection, out hit, rayCastLength, layerMask))//if direction player is heading is into a wall
    //    {
    //        if (!anim.GetBool("WallLean"))//if player is going to wall but not yet in the wall animation
    //        {
    //            freeLookCam.Priority = 10;
    //            anim.SetBool("WallLean", true);
    //            rayCastLength = 1;
    //        }
    //        transform.rotation = Quaternion.FromToRotation(-transform.forward, -hit.normal) * transform.rotation;
    //        //}
    //        //else//if raycast is not hitting a wall
    //        //{
    //        //    if (anim.GetBool("WallLean"))//if player is still in wall animation
    //        //    {
    //        //        freeLookCam.Priority = 10;
    //        //        anim.SetBool("WallLean", false);
    //        //        rayCastLength = 0.5f;

    //        //    }
    //        //}
    //    }
    //}

    //IEnumerator crouchPercent()
    //{
    //    //anim.SetBool("Crouch", !anim.GetBool("Crouch"));

    //    if (anim.GetBool("Crouch"))
    //    {
    //        for (float start = 0; start < 5; start += 0.1f)
    //        {
    //            anim.SetFloat("CrouchPercent", start);
    //            yield return new WaitForSeconds(.01f);
    //        }
    //    }
    //    else
    //    {
    //        for (float start = 5; start > 0; start -= 0.1f)
    //        {
    //            anim.SetFloat("CrouchPercent", start);
    //            yield return new WaitForSeconds(.01f);
    //        }
    //    }
    //}
}


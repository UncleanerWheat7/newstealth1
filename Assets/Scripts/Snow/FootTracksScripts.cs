﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootTracksScripts : MonoBehaviour
{
    private RenderTexture _splatMap;
    public Shader _drawShader;
    private Material _snowMaterial, _drawMaterial;
    //public GameObject terrain;
    public Transform[] foot;


    [Range(0, 2)]
    public float brushSize;
    [Range(0, 50)]
    public float brushStrength;



    RaycastHit _groundHit;
    int layerMask;
    // Start is called before the first frame update
    void Start()
    {
        layerMask = LayerMask.GetMask("Snow");

        _drawMaterial = new Material(_drawShader);        

        _snowMaterial = GetComponent<MeshRenderer>().material;
        _splatMap = new RenderTexture(1024, 1024, 0, RenderTextureFormat.ARGBFloat);
        _snowMaterial.SetTexture("_Splat", _splatMap);
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Transform footTransform in foot)
        {
            if (Physics.Raycast(footTransform.position, Vector3.down,out _groundHit,1,layerMask))
            {                
                _drawMaterial.SetVector("_Coordinate", new Vector4(_groundHit.textureCoord.x, _groundHit.textureCoord.y, 0, 0));
                _drawMaterial.SetFloat("_Strength", brushStrength);
                _drawMaterial.SetFloat("_Size", brushSize);

                RenderTexture temp = RenderTexture.GetTemporary(_splatMap.width, _splatMap.height, 0, RenderTextureFormat.ARGBFloat);
                Graphics.Blit(_splatMap, temp);
                Graphics.Blit(temp, _splatMap, _drawMaterial);
                RenderTexture.ReleaseTemporary(temp);
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class enemyScipt : MonoBehaviour
{
    public NavMeshAgent agent;
    Animator anim;
    bool alive;

    float aim;
    Transform headPos;
    public Transform enemyHead;

    private Vector3 previousPosition;
    Transform goal;
    Transform player;
    Transform lastSeen;
    float timeToWait = 6;
    public LayerMask layersToHit;
    int numberOfPoints;
    float timeToLosePlayer;
    CapsuleCollider capsuleCollider;


    public Transform fireTransform;
    public Transform[] patrolPoints;
    public GameObject bullet;
    public GameObject lastSeenObj;
    public float curSpeed;
    int FOV;
    float sightDistance;
    int currentPatrolPoint;
    public GameObject mesh;

    public enum State { patrol, caution, alert };
    public State state;

    Transform tempTransform;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").transform;
        anim = GetComponent<Animator>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        currentPatrolPoint = 0;
        sightDistance = 15;
        FOV = 65;
        state = State.patrol;
        checkState();
        numberOfPoints = patrolPoints.Length - 1;
        aim = player.lossyScale.y;
        tempTransform = player.gameObject.transform;
        alive = true;

        findPlayerHead();


    }

    // Update is called once per frame
    void Update()
    {
        if (alive)
        {
            playerVisibleTest();
        }
        //checkState();
    }

    private void LateUpdate()
    {
        if (alive)
        {
            move();
        }
        //print(Vector3.Distance(transform.position, goal.transform.position));
        //print(goal.transform.name);
        //print(agent.destination);
        //print(agent.speed);
    }

    void playerVisibleTest()
    {
        Vector3 targetDir = player.position - transform.position;
        float angle = Vector3.Angle(targetDir, transform.forward);//gets angle from enemy front to player position

        Vector3 direction = headPos.transform.position - enemyHead.transform.position;
        Debug.DrawRay(enemyHead.transform.position, direction);

        if (angle <= 60)
        {
            RaycastHit hit;
            if (Physics.Raycast(enemyHead.transform.position, direction, out hit, sightDistance, layersToHit))
            {
                if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Player"))//if there is no obstruction between enemy and player
                {
                    timeToLosePlayer = 1;

                    if (state != State.alert)
                    {
                        state = State.alert;
                        checkState();

                    }
                }
                else if (state == State.alert)
                {
                    if (timeToLosePlayer > 0)
                    {
                        timeToLosePlayer -= Time.deltaTime;
                    }
                    else
                    {
                        lastSeenObj.transform.position = player.transform.position;
                        state = State.caution;
                        checkState();
                    }
                }
            }
        }
        else if (angle > 100)
        {
            if (Vector3.Distance(player.transform.position, transform.position) < 1)
                if (player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Choke") && !anim.GetBool("Choke"))
                {
                    agent.speed = 0;
                    capsuleCollider.enabled = false;

                    transform.eulerAngles = new Vector3(
                        player.transform.eulerAngles.x,
                        player.transform.eulerAngles.y + 27,
                        player.transform.eulerAngles.z
                        );

                    transform.position =
                        player.transform.position + player.transform.forward * 0.226f +
                        player.transform.right * 0.12f;

                    anim.SetTrigger("Choke");
                }
        }
    }

    void checkState()
    {
        if (state == State.patrol)
        {
            agent.speed = 1;
            //agent.stoppingDistance = 1;
            if (patrolPoints.Length != 0)
            {
                goal = patrolPoints[currentPatrolPoint];
            }
            else
            {
                goal = transform;
            }
            mesh.GetComponent<Renderer>().material.color = Color.blue;
        }
        else if (state == State.caution)
        {
            agent.speed = 3;
            //agent.stoppingDistance = 1f;
            goal = lastSeenObj.transform;
            mesh.GetComponent<Renderer>().material.color = Color.yellow;
        }
        else if (state == State.alert)
        {
            agent.speed = 4;
            goal = player.transform;
            mesh.GetComponent<Renderer>().material.color = Color.red;
        }

        agent.SetDestination(goal.transform.position);
    }

    void move()
    {
        if (state == State.patrol)
        {
            if (patrolPoints.Length != 0)
            {
                if (Vector3.Distance(patrolPoints[currentPatrolPoint].transform.position, transform.position) <= 1)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, patrolPoints[currentPatrolPoint].transform.rotation, 0.25f);
                    if (timeToWait <= 0)
                    {
                        if (currentPatrolPoint < numberOfPoints)
                        {
                            currentPatrolPoint++;
                        }
                        else
                        {
                            currentPatrolPoint = 0;
                        }

                        timeToWait = 3;
                        checkState();
                    }
                    else
                    {
                        timeToWait -= Time.deltaTime;
                    }
                }
            }
        }
        else if (state == State.caution)
        {
            if (Vector3.Distance(goal.transform.position, transform.position) <= 1)
            {
                if (timeToWait <= 0)
                {
                    timeToWait = 3;
                    state = State.patrol;
                    checkState();
                }
                else
                {
                    timeToWait -= Time.deltaTime;
                }
            }
        }
        else if (state == State.alert)
        {
            agent.SetDestination(goal.transform.position);
            if (Vector3.Distance(goal.transform.position, transform.position) <= 3)
            {

                agent.speed = 0;
                anim.SetBool("Fire", true);
            }
        }
        setCurrentSpeed();
        anim.SetFloat("Speed", curSpeed);
    }

    void setCurrentSpeed()
    {
        Vector3 curMove = transform.position - previousPosition;
        curSpeed = curMove.magnitude / Time.deltaTime;
        previousPosition = transform.position;
    }
    void attackPlayer()
    {
        transform.LookAt(goal.transform.position);
        anim.SetBool("Fire", true);
    }
    public void fireAgain()
    {
        if (state == State.alert)
        {
            if (Vector3.Distance(goal.transform.position, transform.position) >= 3)
            {
                anim.SetBool("Fire", false);
                checkState();
            }
            else
            {
                transform.LookAt(goal.transform.position);
            }
        }
        else
        {
            anim.SetBool("Fire", false);
            checkState();
        }
    }
    public void spawBullet()
    {
        Instantiate(bullet, fireTransform.position, transform.rotation);
    }
    private void findPlayerHead()
    {
        foreach (Transform eachChild in tempTransform)
        {
            if (eachChild.name == "Hips" || eachChild.name == "Spine" || eachChild.name == "Spine1" || eachChild.name == "Spine2" || eachChild.name == "Neck" || eachChild.name == "Neck1" || eachChild.name == "Head")
            {
                if (eachChild.name == "Head")
                {
                    headPos = eachChild.transform;
                }
                else
                {
                    tempTransform = eachChild.transform;
                    findPlayerHead();
                }
            }
        }
    }

    public void dead()
    {
        alive = false;
    }
}
